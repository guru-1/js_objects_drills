function defaults(obj,obj1)
{
    for(let i in obj1)
    {
        if(typeof obj[i]=="undefined")
        {
            obj[i]=obj1[i]
        }
    }
    return obj
}

export{
    defaults
}